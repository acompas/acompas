import type { SoundsData } from 'src/utils/types'

export default [
  {
    name: 'clara',
    label: 'Clara',
    longLabel: 'Palma clara',
    medias: [
      {
        src: 'clara/clara_1',
        volume: 5,
      },
      {
        src: 'clara/clara_2',
        volume: 5,
      },
      {
        src: 'clara/clara_3',
        volume: 5,
      }
    ]
  },
  {
    name: 'sorda',
    label: 'Sorda',
    longLabel: 'Palma sorda',
    medias: [
      {
        src: 'sorda/sorda_1',
        volume: 0,
      },
      {
        src: 'sorda/sorda_2',
        volume: 0,
      },
      {
        src: 'sorda/sorda_3',
        volume: 0,
      }
    ]
  },
  {
    name: 'pito',
    label: 'Pito',
    medias: [
      {
        src: 'pito/pito_1',
        volume: 0,
      },
      {
        src: 'pito/pito_2',
        volume: 0,
      },
      {
        src: 'pito/pito_3',
        volume: 0,
      }
    ]
  },
  {
    name: 'nudillo',
    label: 'Nudillo',
    medias: [
      {
        src: 'nudillo/nudillo_1',
        volume: 0,
      },
      {
        src: 'nudillo/nudillo_2',
        volume: 5,
      }
    ]
  },
  {
    name: 'cajon',
    label: 'Cajon',
    medias: [
      {
        src: 'cajon/cajon_1',
        volume: -2,
      },
      {
        src: 'cajon/cajon_2',
        volume: -2,
      },
      {
        src: 'cajon/cajon_2',
        volume: -12,
      }
    ]
  },
  {
    name: 'udu',
    label: 'Udu',
    medias: [
      {
        src: 'udu/udu_1',
        volume: -16,
      },
      {
        src: 'udu/udu_2',
        volume: -9,
      },
      {
        src: 'udu/udu_3',
        volume: -4,
      }
    ]
  },
  {
    name: 'jaleo',
    label: 'Jaleo',
    noEighthNotes: true,
    medias: [
      {
        src: 'jaleo/jaleo_1',
        volume: -6,
      },
      {
        src: 'jaleo/jaleo_2',
        volume: -6,
      },
      {
        src: 'jaleo/jaleo_3',
        volume: -6,
      },
      {
        src: 'jaleo/jaleo_4',
        volume: -6,
      },
      {
        src: 'jaleo/jaleo_5',
        volume: -6,
      },
      {
        src: 'jaleo/jaleo_6',
        volume: -6,
      },
      {
        src: 'jaleo/jaleo_7',
        volume: -6,
      },
      {
        src: 'jaleo/jaleo_8',
        volume: -6,
      },
      {
        src: 'jaleo/jaleo_9',
        volume: -6,
      },
      {
        src: 'jaleo/jaleo_10',
        volume: -6,
      },
      {
        src: 'jaleo/jaleo_11',
        volume: -6,
      },
      {
        src: 'jaleo/jaleo_12',
        volume: -6,
      },
      {
        src: 'jaleo/jaleo_13',
        volume: -6,
      },
      {
        src: 'jaleo/jaleo_14',
        volume: -6,
      },
      {
        src: 'jaleo/jaleo_15',
        volume: -6,
      },
      {
        src: 'jaleo/jaleo_16',
        volume: -6,
      },
      {
        src: 'jaleo/jaleo_17',
        volume: -6,
      }
    ]
  },
  {
    name: 'click',
    label: 'Click',
    noEighthNotes: true,
    medias: [
      {
        src: 'click/click_1',
        volume: 15,
      },
      {
        src: 'click/click_2',
        volume: 15,
      }
    ]
  }
] as SoundsData[]
