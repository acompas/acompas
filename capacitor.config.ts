import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'capacitor.acompas.org',
  appName: 'acompas',
  webDir: 'www',
  server: {
    androidScheme: 'https'
  }
};

export default config;
